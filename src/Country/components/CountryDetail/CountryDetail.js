import React, { useContext } from 'react';
import { connect } from 'react-redux';
import { ThemeContext } from 'Context';
import FontAwesome from 'react-fontawesome';
import './CountryDetail.scss';

const handleBackClick = ({ setDetailView }) => {
	setDetailView(false);
};

const getBorderCountriesData = ({ borders, countriesData }) => {
	return countriesData.filter(ele => borders.includes(ele.alpha3Code));
};

const handleBorderClick = ({ data, getCountryDetailData }) => {
	getCountryDetailData(data);
};

const CountryDetail = ({ data, setDetailView, countriesData, getCountryDetailData }) => {
	let {
		flag,
		name,
		nativeName,
		population,
		region,
		subregion,
		capital,
		topLevelDomain,
		currencies,
		languages,
		borders,
	} = data;
	topLevelDomain = topLevelDomain.join(', ');
	currencies = currencies.map(ele => ele.name).join(', ');
	languages = languages.map(ele => ele.name).join(', ');

	const {
		theme: {
			theme: { fontColor, lightBG, borderColor },
		},
	} = useContext(ThemeContext);

	const buttonStyle = {
		backgroundColor: lightBG,
		boxShadow: `1px 1px 2px ${borderColor}`,
		color: fontColor,
	};

	const borderData = getBorderCountriesData({ borders, countriesData });
	console.log(borderData);
	return (
		<div className={`countryDetailContainer container`}>
			<div className={`backButton`} onClick={() => handleBackClick({ setDetailView })} style={buttonStyle}>
				<FontAwesome className="back-arrow" name="arrow-left" />
				{`back`}
			</div>
			<div className={`countrydetailContainer`}>
				<div className={'flagSection'} style={{ backgroundImage: `url(${flag})` }}></div>
				<div className={`otherDetailSection`} style={{ color: fontColor }}>
					<div className={`nameSection`}>{name}</div>
					<div className={`details-section`}>
						<div className={`itemSection`}>
							<div className={`boldItemText`}>{`Native Name: `}</div>
							{nativeName}
						</div>
						<div className={`itemSection`}>
							<div className={`boldItemText`}>{`Population: `}</div>
							{population}
						</div>
						<div className={`itemSection`}>
							<div className={`boldItemText`}>{`Region: `}</div>
							{region}
						</div>
						<div className={`itemSection`}>
							<div className={`boldItemText`}>{`Sub Region: `}</div>
							{subregion}
						</div>
						<div className={`itemSection`}>
							<div className={`boldItemText`}>{`Capital: `}</div>
							{capital}
						</div>
						<div className={`itemSection topLevelDomain`}>
							<div className={`boldItemText`}>{`Top Level Domain: `}</div>
							{topLevelDomain}
						</div>
						<div className={`itemSection`}>
							<div className={`boldItemText`}>{`Currencies: `}</div>
							{currencies}
						</div>
						<div className={`itemSection`}>
							<div className={`boldItemText`}>{`Languages: `}</div>
							{languages}
						</div>
					</div>
					<div className={`borderCountriesSection itemSection`}>
						<div className={`boldItemText`}>{`Border Countries: `}</div>
						<div className={`borderCountriesData`}>
							{borderData.map(ele => (
								<div
									key={ele.alpha3Code}
									className={`borderCountriesButton`}
									style={buttonStyle}
									onClick={() => handleBorderClick({ data: ele, getCountryDetailData })}
								>
									{ele.name}
								</div>
							))}
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

const mapStateToProps = state => {
	const {
		countries: { data },
	} = state;
	const countriesData = data;
	return { countriesData };
};

const mapActionCreator = {};

export default connect(mapStateToProps, mapActionCreator)(CountryDetail);
