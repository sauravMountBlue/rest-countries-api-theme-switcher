import React, { useContext } from 'react';
import { ThemeContext } from 'Context';
import './Cards.scss';

const handleClick = ({ data, setDetailView, getCountryDetailData }) => {
	setDetailView(true);
	getCountryDetailData(data);
};

const Cards = ({ data, setDetailView, getCountryDetailData }) => {
	const { name, capital, region, population, flag } = data;

	const {
		theme: {
			theme: { fontColor, lightBG, borderColor },
		},
	} = useContext(ThemeContext);

	return (
		<div
			className={`countryContainer flexColumn`}
			onClick={() => handleClick({ data, setDetailView, getCountryDetailData })}
		>
			<div className={`flagContainer`} style={{ backgroundImage: `url(${flag})` }}></div>
			<div
				className={`dataContainer flexColumn`}
				style={{ backgroundColor: lightBG, boxShadow: `1px 1px 2px ${borderColor}`, color: fontColor }}
			>
				<div className={`name`}>{name}</div>
				<div className={`details flexColumn`}>
					<div className={`population flexContainer`}>
						<div className={`titleTextOption`}>{`Population: `}</div>
						<div className={`optionData`}>{population}</div>
					</div>
					<div className={`region flexContainer`}>
						<div className={`titleTextOption`}>{`Region: `}</div>
						<div className={`optionData`}>{region}</div>
					</div>
					<div className={`capital flexContainer`}>
						<div className={`titleTextOption`}>{`Capital: `}</div>
						<div className={`optionData`}>{capital}</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Cards;
