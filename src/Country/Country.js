import React, { useEffect, useContext } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { WithLoader } from 'HOC';
import { Cards } from './components';
import { fetchCountries } from 'Actions';
import { ThemeContext } from 'Context';
import './Country.scss';

const Country = ({ fetchCountries, searchData, setDetailView, getCountryDetailData }) => {
	useEffect(() => {
		fetchCountries({ useFilter: false });
	}, []);
	const {
		theme: {
			theme: { fontColor },
		},
	} = useContext(ThemeContext);
	return (
		<div className={`cardContainer container flexWrapSpaceBetween`}>
			{searchData.length > 0 ? (
				searchData.map(ele => (
					<Cards
						data={ele}
						key={ele.alpha3Code}
						setDetailView={setDetailView}
						getCountryDetailData={getCountryDetailData}
					/>
				))
			) : (
				<div className={`noDataContainer`} style={{ color: fontColor }}>
					{`Country Not found matching your search term. Try Different keywords.`}
				</div>
			)}
		</div>
	);
};

const mapStateToProps = state => {
	const {
		countries: { searchData, data },
		stopLoading,
	} = state;
	return { searchData, stopLoading, data };
};

const mapActionCreator = { fetchCountries };

export default compose(connect(mapStateToProps, mapActionCreator), WithLoader)(Country);
