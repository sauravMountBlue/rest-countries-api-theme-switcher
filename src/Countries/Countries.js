import React, { useState, Fragment } from 'react';
import { Header, SearchBar, FilterDropDown } from './components';
import { ThemeContext, themes } from 'Context';
import Country from 'Country';
import { CountryDetail } from 'Country/components';
import './Countries.scss';

const Countries = () => {
	const [theme, setTheme] = useState({ themeType: 'dark', theme: themes.dark });
	const [showDetailView, setDetailView] = useState(false);
	const [countryDetailData, getCountryDetailData] = useState({});
	const {
		theme: { darkBG },
	} = theme;
	return (
		<ThemeContext.Provider value={{ theme, setTheme }}>
			<div className={`countriesContainer`} style={{ backgroundColor: darkBG }}>
				<Header />
				{showDetailView ? (
					<Fragment>
						<CountryDetail
							data={countryDetailData}
							getCountryDetailData={getCountryDetailData}
							setDetailView={setDetailView}
						/>
					</Fragment>
				) : (
					<Fragment>
						<div className={`SearchAndFilterContainer container flexContainerSeacrchNFilter`}>
							<SearchBar />
							<FilterDropDown />
						</div>
						<Country setDetailView={setDetailView} getCountryDetailData={getCountryDetailData} />
					</Fragment>
				)}
			</div>
		</ThemeContext.Provider>
	);
};

export default Countries;
