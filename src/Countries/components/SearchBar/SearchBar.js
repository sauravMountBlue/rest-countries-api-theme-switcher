import React, { useContext, useState } from 'react';
import { connect } from 'react-redux';
import FontAwesome from 'react-fontawesome';
import { ThemeContext } from 'Context';
import { colors } from 'static';
import { fetchCountries } from 'Actions';
import './SearchBar.scss';

const handleChange = ({ setValue, inputValue, fetchCountries, data }) => {
	setValue(inputValue);
	const filteredData = data.filter(ele => ele.name.toLowerCase().includes(inputValue.toLowerCase()));
	fetchCountries({ filteredData, useFilter: true, countryData: data });
};

const SearchBar = ({ data, fetchCountries }) => {
	const [inputValue, setValue] = useState(``);
	const {
		theme: {
			themeType,
			theme: { fontColor, lightBG, borderColor },
		},
	} = useContext(ThemeContext);

	const iconColor = themeType === 'dark' ? fontColor : colors.darkGrey;

	const placeholderClass = themeType === 'dark' ? 'darkPlaceholder' : 'lightPlaceholder';

	return (
		<div
			className={`searchBarContainer`}
			style={{ backgroundColor: lightBG, boxShadow: `1px 1px 2px ${borderColor}`, color: iconColor }}
		>
			<FontAwesome className="search-icon" name="search" />
			<input
				type="text"
				placeholder={`Search for a country...`}
				className={`${placeholderClass} searchBox`}
				value={inputValue}
				onChange={e => handleChange({ setValue, inputValue: e.target.value, fetchCountries, data })}
			/>
		</div>
	);
};

const mapStateToProps = state => {
	const {
		countries: { data },
	} = state;
	return { data };
};

const mapActionCreator = { fetchCountries };

export default connect(mapStateToProps, mapActionCreator)(SearchBar);
