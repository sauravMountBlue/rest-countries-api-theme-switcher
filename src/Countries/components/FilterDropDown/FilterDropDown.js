import React, { useContext, useState } from 'react';
import { connect } from 'react-redux';
import { ThemeContext } from 'Context';
import { ddWhite, ddBlack } from 'images';
import { fetchCountries } from 'Actions';
import './FilterDropDown.scss';

const handleChange = ({ changeOption, changedValue, data, fetchCountries }) => {
	changeOption(changedValue);
	const filteredData = data.filter(ele => ele.region.toLowerCase().includes(changedValue.toLowerCase()));
	fetchCountries({ filteredData, useFilter: true, countryData: data });
};

const FilterDropDown = ({ data, fetchCountries }) => {
	const [selectedOption, changeOption] = useState(`Filter by Region`);

	const {
		theme: {
			themeType,
			theme: { fontColor, lightBG, borderColor },
		},
	} = useContext(ThemeContext);

	const ddImage = themeType === 'dark' ? ddWhite : ddBlack;

	return (
		<select
			className={`selectBox`}
			style={{
				backgroundColor: lightBG,
				boxShadow: `1px 1px 2px ${borderColor}`,
				color: fontColor,
				backgroundImage: `url(${ddImage})`,
			}}
			value={selectedOption}
			onChange={e => handleChange({ changeOption, changedValue: e.target.value, data, fetchCountries })}
		>
			<option disabled>{`Filter by Region`}</option>
			<option>{`Africa`}</option>
			<option>{`America`}</option>
			<option>{`Asia`}</option>
			<option>{`Europe`}</option>
			<option>{`Oceania`}</option>
		</select>
	);
};

const mapStateToProps = state => {
	const {
		countries: { data },
	} = state;
	return { data };
};

const mapActionCreator = { fetchCountries };

export default connect(mapStateToProps, mapActionCreator)(FilterDropDown);
