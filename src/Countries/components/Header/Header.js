import React, { useContext } from 'react';
import FontAwesome from 'react-fontawesome';
import { ThemeContext, themes } from 'Context';
import './Header.scss';

const changeTheme = ({ setTheme, themeType }) => {
	themeType === `dark`
		? setTheme({ themeType: `light`, theme: themes.light })
		: setTheme({ themeType: `dark`, theme: themes.dark });
};

const Header = () => {
	const {
		theme: {
			themeType,
			theme: { fontColor, lightBG, borderColor },
		},
		setTheme,
	} = useContext(ThemeContext);

	const mode = themeType === `dark` ? `Light Mode` : `Dark Mode`;
	return (
		<div
			className={`flexSpaceBetween headerContainer container`}
			style={{ backgroundColor: lightBG, boxShadow: `1px 1px 2px ${borderColor}` }}
		>
			<div className={`flexContainer titleText`} style={{ color: fontColor }}>{`Where in the world?`}</div>
			<div
				className={`flexContainer themeSwitchText`}
				onClick={() => changeTheme({ setTheme, themeType })}
				style={{ color: fontColor }}
			>
				<FontAwesome className="moon-icon" name="moon" />
				{mode}
			</div>
		</div>
	);
};

export default Header;
