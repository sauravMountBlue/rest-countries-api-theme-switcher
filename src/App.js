import React from 'react';
import { Provider } from 'react-redux';
import Countries from 'Countries';
import { store } from 'Store';
import './App.scss';

const App = () => {
	return (
		<Provider store={store}>
			<Countries />
		</Provider>
	);
};

export default App;
