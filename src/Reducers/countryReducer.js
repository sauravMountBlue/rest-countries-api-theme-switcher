import { actionTypes } from 'Actions';

const { FETCH_COUNTRIES } = actionTypes;

const initalState = {
	data: [],
	searchData: [],
	stopLoading: false,
};

const countryReducer = (state = initalState, action) => {
	switch (action.type) {
		case FETCH_COUNTRIES:
			return {
				...state,
				data: action.data,
				stopLoading: action.stopLoading,
				searchData: action.searchData,
			};
		default:
			return state;
	}
};

export default countryReducer;
