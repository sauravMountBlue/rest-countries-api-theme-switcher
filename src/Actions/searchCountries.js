import { actionTypes } from 'Actions';

const { SEARCH_COUNTRIES } = actionTypes;

const searchCountriesAC = ({ data }) => {
	return {
		type: SEARCH_COUNTRIES,
		data,
	};
};

const searchCountries = ({ filteredData }) => dispatch => {
	dispatch(searchCountriesAC({ data: filteredData }));
};

export default searchCountries;
