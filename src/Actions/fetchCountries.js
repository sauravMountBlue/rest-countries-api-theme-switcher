import { actionTypes } from 'Actions';
import axios from 'axios';

const { FETCH_COUNTRIES } = actionTypes;

const fetchCountriesAC = ({ data, searchData }) => {
	return {
		type: FETCH_COUNTRIES,
		data,
		stopLoading: true,
		searchData: searchData,
	};
};

const fetchCountries = ({ filteredData, useFilter, countryData }) => async dispatch => {
	if (useFilter) {
		dispatch(fetchCountriesAC({ searchData: filteredData, data: countryData }));
	} else {
		const APIEndPoint = `https://restcountries.eu/rest/v2/all`;
		try {
			const response = await axios.get(APIEndPoint);
			dispatch(fetchCountriesAC({ data: response.data, searchData: response.data }));
		} catch (error) {
			console.error(error);
		}
	}
};

export default fetchCountries;
