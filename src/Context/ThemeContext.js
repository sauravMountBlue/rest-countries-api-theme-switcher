import { createContext } from 'react';
import { colors } from 'static';
export const themes = {
	dark: {
		lightBG: colors.darkBlue,
		darkBG: colors.veryDarkBlueBG,
		fontColor: colors.white,
		borderColor: colors.veryDarkBlue,
	},
	light: {
		lightBG: colors.white,
		darkBG: colors.veryLightGrey,
		fontColor: colors.veryDarkBlue,
		borderColor: colors.darkGrey,
	},
};

export const ThemeContext = createContext(themes.dark);
