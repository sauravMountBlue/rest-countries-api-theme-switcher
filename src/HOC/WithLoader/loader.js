import React from 'react';
import './loader.scss';

const loader = WrappedComponent => {
	return class extends React.PureComponent {
		constructor(props) {
			super(props);
			this.state = {
				setMinmLoadTime: true,
			};
		}

		componentDidMount() {
			setTimeout(() => {
				this.setState({ setMinmLoadTime: false });
			}, 1000);
		}

		render() {
			const { stopLoading } = this.props;
			const { setMinmLoadTime } = this.state;
			const isLoading = setMinmLoadTime ? setMinmLoadTime : stopLoading;
			return isLoading ? (
				<div className={`loaderContainer`}>
					<div className={`loader`}>
						<div></div>
						<div></div>
					</div>
				</div>
			) : (
				<WrappedComponent {...this.props} />
			);
		}
	};
};

export default loader;
